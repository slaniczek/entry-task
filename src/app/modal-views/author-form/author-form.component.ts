import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {select, Store} from "@ngrx/store";
import {AppStateModel} from "../../models/helper/app-state.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthorDialogDataModel} from "../../models/helper/author-dialog-data.model";
import {$recipeById} from "../../selectors/recipe-detail.selectors";
import {take} from "rxjs/operators";
import {$authorById} from "../../selectors/authors.selectors";
import {RecipeModel} from "../../models/api/recipe.model";
import {IngredientModel} from "../../models/api/ingredient.model";
import {PostRecipeRequestAction, PutRecipeRequestAction} from "../../actions/recipe-detail.actions";
import {PostAuthorRequestAction, PutAuthorRequestAction} from "../../actions/authors.actions";
import {AuthorModel} from "../../models/api/author.model";

enum AuthorFormKey {
    Name = 'Name',
    Email = 'E-mail'
}

@Component({
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.scss']
})
export class AuthorFormComponent implements OnInit {
    readonly AuthorFormKey = AuthorFormKey;
    authorForm: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public readonly dialogData: AuthorDialogDataModel,
              private readonly dialogRef: MatDialogRef<AuthorFormComponent>,
              private readonly store: Store<AppStateModel>,
              private readonly fb: FormBuilder) {}


async ngOnInit(): Promise<void>  {
    const {authorId} = this.dialogData;

    const author = authorId != null ?
        await this.store
            .pipe(select($authorById(authorId)), take(1))
            .toPromise() :
        null;

    this.authorForm = this.fb.group({
        [AuthorFormKey.Name]: [author != null ? author.name : '', Validators.required],
        [AuthorFormKey.Email]: [author != null ? author.email : '', Validators.required]
    });
}


  closeDialog(): void {
      this.dialogRef.close();
  }

  submitAuthor(): void {
      const {value} = this.authorForm;
      const {authorId} = this.dialogData;
      const author: AuthorModel= {
          name: value[AuthorFormKey.Name],
          email: value[AuthorFormKey.Email]
      };
      console.log(author);

      this.store.dispatch(
              new PostAuthorRequestAction(author)
      );
      this.closeDialog();
  }



}
