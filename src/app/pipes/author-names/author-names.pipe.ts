import { Pipe, PipeTransform } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppStateModel } from '../../models/helper/app-state.model';
import {$authorNameById} from '../../selectors/authors.selectors';

@Pipe({
  name: 'appAuthorNames',
  pure: false
})
export class AuthorNamesPipe implements PipeTransform {
  constructor(private readonly store: Store<AppStateModel>) {
  }

  getAuthorNameById: string = (id: string) => {
      var authorNameById: string;
      this.store.pipe(select($authorNameById(id)))
          .subscribe(name => {
              authorNameById = name;
          });

      return authorNameById || "";
  }

  transform(authorIds: string[]): string {
    return authorIds
      .map((id, index) => {
        const prefix = (
          index === 0 ? '' :
          index === authorIds.length - 1 ? ' and ' :
          ', '
        );
        return `${prefix} ${this.getAuthorNameById(id)}`;
      })
      .join('');
  }
}
