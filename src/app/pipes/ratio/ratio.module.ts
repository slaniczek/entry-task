import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RatioPipe } from './ratio.pipe';

@NgModule({
    declarations: [RatioPipe],
    imports: [
        CommonModule
    ],
    exports: [RatioPipe]
})
export class RatioModule { }
