import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ratio'
})
export class RatioPipe implements PipeTransform {
  /*fractionToEntity: {[fraction: string]: string} = {
      "1/4": "\&frac14;",
      "1/2": "\&frac12;",
      "3/4": "\&frac34;"
  };*/

  fractionToEntity = {
        "1/4": "&frac14;",
        "1/2": "&frac12;",
        "3/4": "&frac34;"
  };

  transform(value: any, args?: any): string {
    return this.fractionToEntity[String(value)] || String(value);
  }

}
