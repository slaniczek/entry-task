  export interface AuthorModel {
    id?: string;
    email: string;
    name: string;
    skill?: number;
}
