import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appSkill]'
})
export class SkillDirective implements OnInit {
    @Input('appSkill') skill: number;

    private defaultSkillLevel = 5;
    private defaultClassName = 'high-skill';

    constructor(private el: ElementRef) { }

    private isHigher: boolean = () => Number(this.skill) > this.defaultSkillLevel;

    private do: void = () => {
        this.el.nativeElement.classList.add(this.defaultClassName);
        this.el.nativeElement.title = `Skill is higher then ${this.defaultSkillLevel}`;
    }

    ngOnInit(): void {
        if(this.isHigher()) this.do();
    }
}
