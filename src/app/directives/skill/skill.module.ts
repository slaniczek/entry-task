import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkillDirective} from './skill.directive';

@NgModule({
    declarations: [SkillDirective],
    imports: [
        CommonModule
    ],
    exports: [SkillDirective]
})
export class SkillModule { }
