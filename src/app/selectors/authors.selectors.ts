import { AppStateModel } from '../models/helper/app-state.model';
import { createSelector } from '@ngrx/store';
import { AuthorModel } from '../models/api/author.model';
import { AuthorRowModel } from '../models/helper/author-row.model';
import { $recipeListData } from './recipe-list.selectors';

const $authors = ({authors}: AppStateModel) => authors;

export const $authorsData = createSelector(
    $authors,
    ({data}): AuthorModel[] => data
);

export const $authorRows = createSelector(
    $authorsData,
    $recipeListData,
    (authors, recipes): AuthorRowModel[] => authors
        .map(author => ({
            ...author,
            recipes: recipes.filter(({authorIds}) => authorIds.includes(author.id ? author.id : ""))
        }))
);


export const $authorById = (id: string) => createSelector(
    $authorsData,
    (authors): AuthorModel => authors[id]
);


export const $authorNameById = (id: string) => createSelector(
    $authorsData,
    (authors): string => authors.find((author: AuthorModel): boolean => (author.id == id)).name || ""
)